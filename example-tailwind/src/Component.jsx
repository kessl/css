import React from 'react'
import { PlantCard } from './PlantCard.jsx'
import './global.css'

const TestComponent = () => (
  <div className="m-10 flex w-64">
    <PlantCard />
  </div>
)

export default TestComponent
