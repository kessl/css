import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'

const semantic = `.pink-rounded-button {
  background: pink;
  border-radius: 5px;
  cursor: pointer;
}
`
const semanticHtml = `<button class="pink-rounded-button">
  click me
</button>`

const utility = `.bg-pink {
  background: pink;
}

.rounded {
  border-radius: 5px;
}

.pointer {
  cursor: pointer; 
}
`
const utilityHtml = `<button class="bg-pink rounded pointer">
  click me
</button>`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({
  code: { semantic, utility, semanticHtml, utilityHtml },
}) => (
  <MainLayout fullWidth>
    <div className="flex justify-start w-full">
      <div>
        <h2 className="font-heading text-6 text-center mb-30">semantic</h2>
        <pre className="hljs" dangerouslySetInnerHTML={{ __html: semantic }} />
        <pre className="hljs" dangerouslySetInnerHTML={{ __html: semanticHtml }} />
      </div>
      {/* <div className="w-100" />
      <div className="flex-1">
        <h2 className="font-heading text-6 text-center mb-30">utility</h2>
        <pre className="hljs" dangerouslySetInnerHTML={{ __html: utility }} />
        <pre className="hljs" dangerouslySetInnerHTML={{ __html: utilityHtml }} />
      </div> */}
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        semantic: hljs.highlightAuto(semantic).value,
        utility: hljs.highlightAuto(utility).value,
        semanticHtml: hljs.highlightAuto(semanticHtml, ['html']).value,
        utilityHtml: hljs.highlightAuto(utilityHtml, ['html']).value,
      },
    },
  }
}

export default Slide
