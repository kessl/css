import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'
import { Code } from 'components'

const install = `$ yarn add -D styled-components
              babel-plugin-styled-components`

const babelrc = `{
  "presets": [
    "@babel/preset-env",
    "@babel/preset-react"
  ],
  "plugins": ["babel-plugin-styled-components"]
}
`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { install, babelrc } }) => (
  <MainLayout fullWidth>
    <h1 className="font-heading text-5 text-center mb-100">styled components setup</h1>
    <div className="w-full flex">
      <div className="w-1/2">
        <Code code={install} />
      </div>
      <div className="w-50" />
      <div className="w-1/2">
        <Code title=".babelrc" code={babelrc} />
      </div>
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        install: hljs.highlightAuto(install, ['js']).value,
        babelrc: hljs.highlightAuto(babelrc).value,
      },
    },
  }
}

export default Slide
