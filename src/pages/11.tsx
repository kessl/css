import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'
import { Code } from 'components'

const install = `$ yarn add -D tailwindcss postcss-loader
              css-loader style-loader`
const postcss = `module.exports = {
  plugins: [require('tailwindcss')],
}
`
const css = `@tailwind base;

@tailwind components;

@tailwind utilities;
`
const tailwind = `module.exports = {
  purge: [],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
`
const webpack = `{
  test: /\.css$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'style-loader',
    },
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1,
      },
    },
    {
      loader: 'postcss-loader',
    },
  ],
},
`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { tailwind, postcss, css, webpack } }) => (
  <MainLayout fullWidth>
    <h1 className="font-heading text-5 text-center">Tailwind setup</h1>
    <div className="w-full flex">
      <div className="w-1/2">
        <Code code={install} />
        <Code title="webpack.config.js" code={webpack} />
      </div>
      <div className="w-50" />
      <div className="w-1/2">
        {/* <Code title="postcss.config.js" code={postcss} />
        <Code title="global.css" code={css} />
        <Code title="tailwind.config.js" code={tailwind} /> */}
      </div>
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        postcss: hljs.highlightAuto(postcss, ['js']).value,
        css: hljs.highlightAuto(css).value,
        tailwind: hljs.highlightAuto(tailwind, ['js']).value,
        webpack: hljs.highlightAuto(webpack, ['js']).value,
      },
    },
  }
}

export default Slide
