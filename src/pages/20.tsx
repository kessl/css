import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'
import { Code } from 'components'

const button = `const Button = styled.button\`
  width: 100%;
  padding: 0.5rem;
  background-color: #38a169;
  border: 4px solid #2f855a;
  border-radius: 0.25rem;
  font-family: monospace;
  font-size: 1rem;
  color: #ffffff;
  text-align: center;
  cursor: pointer;
\``

const button2 = `const Button = styled.button\`
width: 100%;
padding: 0.5rem;
background:
  \${({ kind }) => kind === 'primary' ? '#38a169' : 'none'};
border: 4px solid #2f855a;
border-radius: 0.25rem;
font-family: monospace;
font-size: 1rem;
color:
  \${({ kind }) => kind === 'primary' ? '#fff' : '#333'};
text-align: center;
cursor: pointer;
\``

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { button, button2 } }) => (
  <MainLayout fullWidth>
    <div className="w-full flex">
      <div className="w-1/2 flex justify-center items-center">
        <Code code={button} />
      </div>
      <div className="w-1/2 flex justify-center">
        <Code code={button2} />
      </div>
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        button: hljs.highlightAuto(button, ['jsx']).value,
        button2: hljs.highlightAuto(button2, ['jsx']).value,
      },
    },
  }
}

export default Slide
