import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'

const tw = `import './global.css'

const TestComponent = () => (
  <div>
    <h1 className="text-lg text-green">styled title</h1>
  </div>
)
`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { tw } }) => (
  <MainLayout fullWidth>
    <h1 className="font-heading text-5 text-center mb-50">Tailwind</h1>
    <div className="flex justify-center w-full">
      <pre className="hljs" dangerouslySetInnerHTML={{ __html: tw }} />
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        tw: hljs.highlightAuto(tw, ['jsx']).value,
      },
    },
  }
}

export default Slide
