import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'

const Slide: NextPage = () => (
  <MainLayout>
    <h1 className="font-heading text-8">
      CSS:
      <br /> Semantic or utility
    </h1>
  </MainLayout>
)

export default Slide
