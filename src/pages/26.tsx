import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'

const Slide: NextPage = () => (
  <MainLayout>
    <h1 className="font-heading text-center my-100 text-7">thank you</h1>
    <div className="flex flex-col space-y-30">
      <a href="https://styled-components.com/docs/basics#motivation">
        styled-components.com/docs/basics#motivation
      </a>
      <a href="https://tailwindcss.com/#what-is-tailwind">tailwindcss.com/#what-is-tailwind</a>
      <a href="https://adamwathan.me/css-utility-classes-and-separation-of-concerns/">
        adamwathan.me/css-utility-classes-and-separation-of-concerns
      </a>
      <div className="text-center pt-50">
        <p>
          you can find this at <a href="https://css.bitgate.cz">css.bitgate.cz</a>
        </p>
      </div>
    </div>
  </MainLayout>
)

export default Slide
