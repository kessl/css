import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import Card from 'images/card.png'

const Slide: NextPage = () => (
  <MainLayout>
    <img src={Card} className="h-740" />
  </MainLayout>
)

export default Slide
