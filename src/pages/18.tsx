import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'

const Slide: NextPage = () => (
  <MainLayout fullWidth>
    <h1 className="font-heading text-5 text-center mb-50">reusing components</h1>
  </MainLayout>
)
export default Slide
