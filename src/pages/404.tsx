import { NextPage } from 'next'
import { useEffect } from 'react'
import { useRouter } from 'next/dist/client/router'

const NotFoundPage: NextPage = () => {
  const router = useRouter()
  useEffect(() => {
    router.push('/1')
  }, [])

  return null
}

export default NotFoundPage
