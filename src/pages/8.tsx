import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'

const cs = `import styled from 'styled-components'

const Title = styled.h1\`
  font-size: 1.5em;
  color: green;
\`

const TestComponent = () => (
  <div>
    <Title>styled title</Title>
  </div>
)
`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { cs } }) => (
  <MainLayout fullWidth>
    <h1 className="font-heading text-5 text-center mb-50">styled components</h1>
    <div className="flex justify-center w-full">
      <pre className="hljs" dangerouslySetInnerHTML={{ __html: cs }} />
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        cs: hljs.highlightAuto(cs, ['jsx']).value,
      },
    },
  }
}

export default Slide
