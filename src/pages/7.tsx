import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'
import BadDK from 'images/dk.png'

const bad = `<div className="flex flex-col lg:flex-row justify-start items-end
  flex-shrink md:flex-shrink-0 mt-20 mb-30 lg:my-0 p-20">
  <div className="flex-1 flex flex-col md:flex-row justify-start lg:justify-between
    items-start md:items-center flex-grow p-20 lg:p-0 lg:pb-20">
    boo hoo
  </div>
</div>
`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { bad } }) => (
  <MainLayout fullWidth>
    <div className="flex justify-center w-full">
      <div className="relative">
        <pre className="hljs" dangerouslySetInnerHTML={{ __html: bad }} />
        <img src={BadDK} className="absolute right-0 bottom-0" />
      </div>
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        bad: hljs.highlightAuto(bad, ['html']).value,
      },
    },
  }
}

export default Slide
