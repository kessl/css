import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import Bad from 'images/bad.png'

const Slide: NextPage = () => (
  <MainLayout>
    <img src={Bad} />
  </MainLayout>
)

export default Slide
