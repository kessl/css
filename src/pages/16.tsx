import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'
import { Code } from 'components'

const twCard = `export const PlantCard = () => (
  <div className="border border-gray-300 rounded-lg shadow-lg p-6">
    <div className="flex justify-center">
      <img src={plant} className="w-32" />
    </div>
    <div className="mt-4">
      <p className="font-mono font-bold text-lg">Monstera deliciosa</p>
      <p className="mt-2 mb-5 font-light">
        Also known as the Swiss Cheese plant, it is a species of flowering plant native to tropical
        forests of southern Mexico, south to Panama.
      </p>
      <button className="w-full p-2 bg-green-600 border-4 border-green-700 rounded font-mono text-white text-center">
        buy immediately
      </button>
    </div>
  </div>
)`

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { twCard } }) => (
  <MainLayout fullWidth>
    <div className="w-full flex justify-center">
      <Code code={twCard} />
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        twCard: hljs.highlightAuto(twCard, ['jsx']).value,
      },
    },
  }
}

export default Slide
