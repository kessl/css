import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import hljs from 'highlight.js'
import { Code } from 'components'

const scCard = `export const PlantCard = () => (
  <Card>
    <ImageContainer>
      <Image src={plant} />
    </ImageContainer>
    <CardContent>
      <CardTitle>Monstera deliciosa</CardTitle>
      <CardText>
        Also known as the Swiss Cheese plant, it is a species of flowering plant native to tropical
        forests of southern Mexico, south to Panama.
      </CardText>
      <Button>buy immediately</Button>
    </CardContent>
  </Card>
)

const Card = styled.div\`
  border: 1px solid #e2e8f0;
  border-radius: 0.5rem;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
              0 4px 6px -2px rgba(0, 0, 0, 0.05);
  padding: 1.5rem;
\`

const ImageContainer = styled.div\`
  display: flex;
  justify-content: center;
\``

const scStyles = `const Image = styled.img\`
  width: 8rem;
\`

const CardContent = styled.div\`
  margin-top: 1rem;
\`

const CardTitle = styled.p\`
  font-family: monospace;
  font-weight: bold;
  font-size: 1.125rem;
\`

const CardText = styled.p\`
  font-family: sans-serif;
  margin: 0.5rem 0 1.25rem 0;
  font-weight: 300;
  line-height: 1.5;
\`

const Button = styled.button\`
  width: 100%;
  padding: 0.5rem;
  background-color: #38a169;
  border: 4px solid #2f855a;
  border-radius: 0.25rem;
  font-family: monospace;
  font-size: 1rem;
  color: #ffffff;
  text-align: center;
  cursor: pointer;
\``

interface SlideWithCodeProps {
  code: Record<string, string>
}

const Slide: NextPage<SlideWithCodeProps> = ({ code: { scCard, scStyles } }) => (
  <MainLayout fullWidth>
    <div className="w-full flex">
      <div className="w-1/2 flex flex-col items-start">
        <Code code={scCard} />
      </div>
      <div className="w-50" />
      <div className="w-1/2">
        <Code code={scStyles} />
      </div>
    </div>
  </MainLayout>
)

export function getStaticProps() {
  return {
    props: {
      code: {
        scCard: hljs.highlightAuto(scCard, ['jsx']).value,
        scStyles: hljs.highlightAuto(scStyles, ['jsx']).value,
      },
    },
  }
}

export default Slide
