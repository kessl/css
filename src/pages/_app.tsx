import { AppProps } from 'next/app'
import React from 'react'
import 'highlight.js/styles/monokai-sublime.css'
import 'utils/global.css'

const CSSApp = ({ Component, pageProps }: AppProps) => <Component {...pageProps} />

export default CSSApp
