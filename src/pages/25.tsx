import { NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'containers'
import Intellisense from 'images/intellisense.png'
import IntellisenseSC from 'images/vscode_sc.png'

const Slide: NextPage = () => (
  <MainLayout>
    <img src={Intellisense} className="pb-30 max-w-xl" />
    <div className="text-center">
      <a href="https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss">
        bradlc.vscode-tailwindcss
      </a>
    </div>

    <div className="text-center flex flex-col items-center mt-120">
      <img src={IntellisenseSC} className="pb-30" />
      <a href="https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components">
        jpoissonnier.vscode-styled-components
      </a>
    </div>
  </MainLayout>
)

export default Slide
