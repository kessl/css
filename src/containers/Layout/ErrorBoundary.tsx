import React, { ErrorInfo } from 'react'

interface ErrorBoundaryProps {}

interface ErrorBoundaryState {
  error?: Error
}

export class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
  state: ErrorBoundaryState = {}

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error({ error, errorInfo })
  }

  static getDerivedStateFromError(error: Error) {
    return { error }
  }

  render() {
    if (this.state.error) {
      return (
        <div className="flex flex-row justify-center items-center w-full h-300">
          There was an error. Please try reloading the page.
        </div>
      )
    }

    return this.props.children
  }
}
