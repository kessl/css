import React, { useEffect, useCallback } from 'react'
import { Meta, ErrorBoundary } from '.'
import { useRouter } from 'next/dist/client/router'
import cn from 'classnames'

interface MainLayoutProps {
  title?: string
  fullWidth?: boolean
}

export const MainLayout: React.FC<MainLayoutProps> = ({ title, fullWidth, children }) => {
  const router = useRouter()
  const currentSlide = +router.asPath.replace(/\//g, '')

  const handleKeyUp = useCallback(
    (e: KeyboardEvent) => {
      e.preventDefault()

      if (e.isComposing) {
        return
      }

      if (e.key === 'ArrowLeft' || e.key === 'Backspace') {
        router.push(`/${currentSlide - 1}`)
        return
      }

      router.push(`/${currentSlide + 1}`)
    },
    [router]
  )

  useEffect(() => {
    document.addEventListener('keyup', handleKeyUp)
    return () => document.removeEventListener('keyup', handleKeyUp)
  }, [])

  return (
    <>
      <Meta title={title!} />
      <div
        className={cn(
          'h-full flex justify-center items-center overflow-hidden',
          fullWidth && 'w-full'
        )}
      >
        <main className={cn(fullWidth ? 'w-full' : 'm-50')}>
          <ErrorBoundary>{children}</ErrorBoundary>
        </main>
      </div>
    </>
  )
}

MainLayout.defaultProps = {
  title: 'CSS: Semantic or utility',
}
