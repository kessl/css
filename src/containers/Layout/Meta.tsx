import Head from 'next/head'
import React from 'react'

interface MetaProps {
  title: string
}

export const Meta: React.FC<MetaProps> = props => (
  <Head>
    <title>{props.title}</title>
    <meta name="description" content="A short comparison of two approaches to styling" />
    <meta name="viewport" content="width=device-width" />

    <meta property="og:title" content="CSS: Semantic or utility?" />
    <meta property="og:url" content={process.env.NEXT_PUBLIC_BASE_URL} />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="A short comparison of two approaches to styling" />
  </Head>
)
