import React from 'react'

interface CodeProps {
  title?: string
  code: string
}

export const Code: React.FC<CodeProps> = ({ title, code }) => (
  <>
    <div className="mt-30 mb-10 font-mono text-code">{title ?? <>&nbsp;</>}</div>
    <pre
      className="hljs whitespace-pre-wrap text-code"
      dangerouslySetInnerHTML={{ __html: code }}
    />
  </>
)
