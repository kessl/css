const withImages = require('next-optimized-images')

module.exports = phase =>
  withImages({
    // TODO: install image optimization packages
    // https://github.com/cyrilwanner/next-optimized-images#optimization-packages
    optimizeImages: false,

    // needed for index pages due to GitLab directory redirects
    exportTrailingSlash: true,

    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
      return config
    },

    poweredByHeader: false,
  })
