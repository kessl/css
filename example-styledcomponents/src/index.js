import React from 'react'
import ReactDOM from 'react-dom'
import Component from './Component.jsx'

const root = document.getElementById('root')
ReactDOM.render(<Component />, root)
