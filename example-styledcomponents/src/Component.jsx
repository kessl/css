import React from 'react'
import { PlantCard } from './PlantCard.jsx'

const TestComponent = () => (
  <div style={{ margin: '2.5rem', width: '16rem', display: 'flex' }}>
    <PlantCard />
  </div>
)

export default TestComponent
